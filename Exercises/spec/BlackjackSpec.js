

describe("Blackjack calPoints unit tests",function(){
    var hand1 = [
        {
            val: 10,
            displayVal:'10',
            suit:'clubs'
        },
        {
            val: 7,
            displayVal:'7',
            suit:'clubs'
        }
    ];
    var hand2 = [
        {
            val: 11,
            displayVal:'Ace',
            suit:'spades'
        },
        {
            val: 9,
            displayVal:'9',
            suit:'clubs'
        }
    ];
    var hand3 = [
        {
            val: 10,
            displayVal:'10',
            suit:'clubs'
        },
        {
            val: 6,
            displayVal:'6',
            suit:'clubs'
        },
        {
            val: 11,
            displayVal:'Ace',
            suit:'spades'
        }
    ];
    var result1 = {
        total:17,
        isSoft:false
    };
    var result2 = {
        total:20,
        isSoft:true
    };
    var result3 = {
        total:17,
        isSoft:false
    };
    it('expect calcPoints to equal 17 for hand1 with isSoft = false', function(){
        expect(calcPoints(hand1)).toEqual(result1);
    });
    it('expect calcPoints to equal 20 for hand2 with isSoft = true', function(){
        expect(calcPoints(hand2)).toEqual(result2);
    });
    it('expect calcPoints to equal 17 for hand3 with isSoft = false', function(){
        expect(calcPoints(hand3)).toEqual(result3);
    });
    
});
