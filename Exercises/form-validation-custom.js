window.onload = function(){
    let form = document.getElementById('connect-form');
    form.onsubmit = function(event){
        let elements = document.getElementsByClassName('validate-input');
        let submit = true;
        for(let i = 0; i < elements.length; i++) {
            let x = elements[i].parentNode;
            let y = x.getElementsByClassName('error')[0];
            if(elements[i].type === 'text' && elements[i].value.length < 3){
                submit = false;
                x.classList.add('invalid');
                y.textContent = `Please enter a valid value for ${elements[i].name}`;
                console.log('bad input');
            }else{
                y.textContent = '';
                x.classList.remove('invalid');
            }
            if(elements[i].type === 'email'){
                var email = elements[i].value;
                var emailRegex = /\w+@\w+\.\w+/;
                var result = emailRegex.test(email);
                if(result === false){
                    x.classList.add('invalid');
                    submit = false;
                    y.textContent = `Please enter a valid value for ${elements[i].name}`;
                    console.log('bad input');
                }else{
                    y.textContent = '';
                    x.classList.remove('invalid');
                }
            }
        }
        if(submit === false){
            event.preventDefault();
        }

    };
};