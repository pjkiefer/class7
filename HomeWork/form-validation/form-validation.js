window.onload = function(){
    let form = document.getElementById('connect-form');
    form.onsubmit = function(event){
        let elements = document.getElementsByClassName('validate-input');
        let submit = true;
        for(let i = 0; i < elements.length; i++) {
            let x = elements[i].parentNode;
            let y = x.getElementsByClassName('error')[0];
            if(elements[i].type === 'text' && elements[i].value.length < 3){
                submit = false;
                x.classList.add('invalid');
                y.textContent = `Please enter a valid value for ${elements[i].name}`;
                console.log('bad input');
            }else{
                y.textContent = '';
                x.classList.remove('invalid');
            }
            if(elements[i].type === 'email'){
                var email = elements[i].value;
                var emailRegex = /\w+@\w+\.\w+/;
                var result = emailRegex.test(email);
                if(result === false){
                    x.classList.add('invalid');
                    submit = false;
                    y.textContent = `Please enter a valid value for ${elements[i].name}`;
                    console.log('bad input');
                }else{
                    y.textContent = '';
                    x.classList.remove('invalid');
                }
            }
            if(elements[i].type === 'textarea' && elements[i].value.length < 10){
                x.classList.add('invalid');
                submit = false;
                y.textContent = `Please enter a valid value for ${elements[i].name}`;
                console.log('bad input');
            }else if(elements[i].type === 'textarea' && elements[i].value.length >= 10){
                y.textContent = '';
                x.classList.remove('invalid');
            }
        }
        let check = checkContactReason();
        if(check === false){
            submit = false;
        }
        if(submit === false){
            event.preventDefault();
        }else{
            let select = document.getElementById("reason-options");
            localStorage.setItem('contact_type',select.value);
        }

    };
    let select = document.getElementById("reason-options");
    select.onchange = function() {
        let parentDiv = document.getElementById('job-opportunity');
        let talkDiv = document.getElementById('code-language');
        if(select.value === 'Job'){
            parentDiv.classList.remove('displayHidden');
        }else{
            parentDiv.classList.add('displayHidden');
        }
        if(select.value === 'Talk'){
            talkDiv.classList.remove('displayHidden');
        }else{
            talkDiv.classList.add('displayHidden');
        }
    };
    function checkContactReason(){
        let success = true;
        if(select.value === 'Job'){
            let reasons = document.getElementsByClassName('validate-input2');
            for(let i = 0; i < reasons.length; i++){
                let x = reasons[i].parentNode;
                let y = x.getElementsByClassName('error')[0];
                if(reasons[i].value === ''){
                    x.classList.add('invalid');
                    y.textContent = `Please enter a valid value for ${reasons[i].name}`;
                    success = false;
                }
                else if(reasons[i].value.length > 0){
                    if(reasons[i].name === 'company-url'){
                        var urlRegex = /https?\:\/\/.+\..+/;
                        var result = urlRegex.test(reasons[i].value);
                        if(result === false){
                            x.classList.add('invalid');
                            y.textContent = `Please enter a valid value for ${reasons[i].name}`;
                            success = false;
                        }
                        else{
                            y.textContent = '';
                            x.classList.remove('invalid');
                        }

                    }else{
                        y.textContent = '';
                        x.classList.remove('invalid');
                    }
                    
                }
            }
            return success;
        }
    }
};