// Kept the starter Jasmine Player Spec Code
// Update this!

describe("Blackjack card game dealerShouldDraw tests", function() {
 
  var hand1 = [
    {
        val: 10,
        displayVal:'10',
        suit:'clubs'
    },
    {
        val: 9,
        displayVal:'9',
        suit:'clubs'
    }
  ];
  var hand2 = [
    {
        val: 11,
        displayVal:'Ace',
        suit:'spades'
    },
    {
        val: 6,
        displayVal:'6',
        suit:'clubs'
    }
  ];
  var hand3 = [
    {
      val: 10,
      displayVal:'10',
      suit:'spades'
    },
    {
      val: 7,
      displayVal:'7',
      suit:'clubs'
    }
  ];
  var hand4 = [
    {
        val: 2,
        displayVal:'2',
        suit:'clubs'
    },
    {
        val: 4,
        displayVal:'4',
        suit:'clubs'
    },
    {
        val: 2,
        displayVal:'2',
        suit:'spades'
    },
    {
      val: 5,
      displayVal:'5',
      suit:'spades'
    }
  ];
  it("10 and 9 passed in and dealer should not draw (false)", function() {
    expect(dealerShouldDraw(hand1)).toEqual(false);
  });
  it("Ace and 6 passed in - the dealer should draw (true)", function(){
    expect(dealerShouldDraw(hand2)).toEqual(true);
  });
  it("10 and 7 passed in - the dealer should not draw (false)", function(){
    expect(dealerShouldDraw(hand3)).toEqual(false);
  });
  it("2, 4, 2 and 5 passed in - the dealer should draw (true)", function(){
    expect(dealerShouldDraw(hand4)).toEqual(true);
  });
});
